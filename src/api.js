const fs = require('fs').promises;
const axios = require("axios").default;
const openpgp = require('openpgp');

const randomBytes = require('util').promisify(require('crypto').randomBytes);

const base = 'https://in-n-out.team6479.org/api/v1';

let privkey = null;

async function get_privkey() {
  return await fs.readFile('.in-n-out/discord/bot_eid')
      .then(data => axios.get(base + "/instant/entities/" + data.toString() + "/key/priv"))
      .then(keyfile => (openpgp.key.readArmored(keyfile.data)))
      .then(keys => keys.keys[0])
      .then(key => fs.readFile('.in-n-out/discord/bot_privkey_pass')
          .then(pass => key.decrypt(pass).then(() => key)));
}

async function create_moment(event, character, data, timestamp) {
  return JSON.stringify({
    v: '1.0.0',
    event: event,
    timestamp: timestamp,
    actor: await fs.readFile('.in-n-out/discord/bot_eid').then(data => data.toString()),
    character: character,
    data: data
  });
}

async function sign_text(text) {
  if (!privkey) {
    privkey = await get_privkey();
  }
  return await openpgp.sign({
    message: openpgp.cleartext.fromText(text),
    privateKeys: [privkey]
  }).then(s => s.data);
}

async function gen_mid() {
  // TODO: check for duplicates
  return randomBytes(16).then(buf => buf.toString('hex').toUpperCase());
}

async function submit_moment(mid, data) {
  console.log('Submitting ' + mid);
  return await axios.post(base + "/moment/" + mid, data);
  
}

async function create_and_full_submit_moment(event, character, data, timestamp) {
  create_moment(event, character, data, timestamp).then(m => sign_text(m)).then(s => gen_mid().then(m => submit_moment(m, s)));
}

async function create_entity(eid) {
  create_and_full_submit_moment('entity', eid, null, Math.floor(Date.now() / 1000));
}

async function set_name(eid, name, ts_seq_hack) {
  // ts_seq_hack: add one to timestamp to ensure that it falls after creation of entity
  create_and_full_submit_moment('name', eid, name, Math.floor(Date.now() / 1000) + (ts_seq_hack ? 1 : 0));
}

async function signin(eid, ts, location) {
  create_and_full_submit_moment('signin', eid, location, ts);
}

async function signout(eid, ts, approver) {
  create_and_full_submit_moment('signout', eid, approver, ts);
}

async function cancel(eid, ts) {
  create_and_full_submit_moment('cancel', eid, null, ts);
}

module.exports = {
  create_entity: create_entity,
  set_name: set_name,
  signin: signin,
  signout: signout,
  cancel: cancel
}