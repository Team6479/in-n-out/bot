const { AkairoClient, CommandHandler } = require("discord-akairo");
const fs = require('fs').promises;
const status = require('./status');

class InNOutBot extends AkairoClient {
    constructor() {
        super({
            ownerID: '184781742643609601'
        }, {
            disableMentions: 'everyone'
        });

        this.commandHandler.loadAll();
    };

    commandHandler = new CommandHandler(this, {
        directory: 'src/commands/',
        prefix: '++'
    });
}

const client = new InNOutBot();
fs.readFile('.in-n-out/discord/discord_token').then(token => client.login(token.toString()));
console.log("Logged in.");

client.on("ready", () => {
    console.log("Online.");
    setInterval(() => status.setNewStatus(client), 30000);
});

module.exports = InNOutBot;