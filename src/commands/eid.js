const { Command } = require('discord-akairo');
const eidb = require('../eidb');

class CommandEid extends Command {
    constructor() {
        super('eid', {
            aliases: ['eid', 'id'],
            args: [
                {
                    id: 'eid',
                    type: 'string',
                    default: ""
                }
            ]
        })
    }

    exec(msg, args) {
        var reply = null;
        if (args.eid != "") { // could be valid ig
            if (args.eid.match(/^[0-9]{8}$/)) { // normal student eid
                reply = msg.reply('You selected the Entity ID ' + args.eid + '. Is this correct?');
            } else if (args.eid.match(/^s[0-9]{8}$/i)) { // leading S, probably bad
                reply = msg.reply('If your Entity ID is your student ID, don\'t include a leading S. Are you sure ' + args.eid + ' is correct?');
            } else { // unrecognized
                reply = msg.reply('Your Entity ID does not appear to be a CdS student ID. Are you sure ' + args.eid + ' is correct?');
            }
            const user_filter = (reaction, user) => ['🇾', '🇳'].includes(reaction.emoji.name) && user.id === msg.author.id;
            const officer_filter = (reaction, user) => reaction.emoji.name === '✅' && msg.guild.member(user).roles.cache.some(r => r.name === 'Officer');
            reply.then(m => {
                m.react('🇾').then(() => m.react('🇳'));
                m.awaitReactions(user_filter, {max: 1, time: 60000, errors: ['time']}).then(c => {
                    const r = c.first();
                    if (r.emoji.name === '🇾') { // yes
                        m.reactions.removeAll();
                        m.edit('An admin will approve your Entity ID (' + args.eid + ') shortly.');
                        m.react('✅');
                        m.awaitReactions(officer_filter, {max: 1, time: 24 * 3600 * 1000, errors: ['time']}).then(oc => {
                            m.reactions.removeAll();
                            m.edit('Working...');
                            eidb.set(msg.author.id, args.eid).then(() => {
                                m.edit('Your Entity ID has been set to ' + args.eid + '.');
                            }).catch(() => {
                                m.edit('Failed to set Entity ID. Feel free to ping the programmers.');
                            });
                        }).catch(oc => {
                            m.reactions.removeAll();
                            m.edit('An officer did not approve the requested action within one day.');
                        });
                    } else { // no
                        m.reactions.removeAll();
                        m.edit('Canceled. Your Entity ID has not been changed.');
                    }
                }).catch(c => {
                    m.reactions.removeAll();
                    m.edit('Please react within one minute with either Y or N.');
                });
            });
        } else {
            eidb.get(msg.author.id).then(eid => {
                msg.reply('Your Entity ID is ' + eid + '.');
            }).catch(() => {
                msg.reply('Failed to retrieve Entity ID. This is most likely because you have not set one. If this is a mistake, feel free to ping the programmers.');
            });
        }
    }
}

module.exports = CommandEid;