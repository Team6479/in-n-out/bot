const { Command } = require('discord-akairo');

class CommandPing extends Command {
    constructor() {
        super('ping', {
            aliases: ['ping', 'e']
        })
    }

    exec(msg) {
        msg.reply('pong');
    }
}

module.exports = CommandPing;
