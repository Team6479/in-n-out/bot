const { Command } = require('discord-akairo');
const eidb = require('../eidb');
const api = require('../api');

class CommandEid extends Command {
    constructor() {
        super('setup', {
            aliases: ['setup'],
            args: [
                {
                    id: 'eid',
                    type: 'string',
                    default: ''
                },
                {
                    id: 'name',
                    type: 'string',
                    default: ''
                },
                {
                    id: 'name2',
                    type: 'string',
                    default: ''
                },
                {
                    id: 'name3',
                    type: 'string',
                    'default': ''
                }
            ]
        })
    }

    exec(msg, args) {
        let name = [args.name, args.name2, args.name3].join(' ').trim();
        if (args.eid != "") { // could be valid ig
            if (args.eid.match(/^[0-9]{8}$/)) { // normal student eid
                var reply = msg.reply('You selected the Entity ID ' + args.eid + ' and the name "' + name + '". Is this correct?');
                const user_filter = (reaction, user) => ['🇾', '🇳'].includes(reaction.emoji.name) && user.id === msg.author.id;
                const officer_filter = (reaction, user) => reaction.emoji.name === '✅' && msg.guild.member(user).roles.cache.some(r => r.name === 'Officer');
                reply.then(m => {
                    m.react('🇾').then(() => m.react('🇳'));
                    m.awaitReactions(user_filter, {max: 1, time: 60000, errors: ['time']}).then(c => {
                        const r = c.first();
                        if (r.emoji.name === '🇾') { // yes
                            m.reactions.removeAll();
                            m.edit('An admin will approve your Entity ID (' + args.eid + ') and name (' + name + ') shortly.');
                            m.react('✅');
                            m.awaitReactions(officer_filter, {max: 1, time: 24 * 3600 * 1000, errors: ['time']}).then(oc => {
                                m.reactions.removeAll();
                                m.edit('Working...');
                                api.create_entity(args.eid).then(() => {
                                    api.set_name(args.eid, name, true).then(() => {
                                      eidb.set(msg.author.id, args.eid).then(() => {
                                          m.edit('An Entity has been created with the ID ' + args.eid + ' and the name "' + name + '".');
                                      }).catch(() => {
                                          m.edit('Failed to set your Entity ID. Feel free to ping the programmers.');
                                      });
                                    }).catch(() => {
                                        m.edit('Failed to set Entity name. Feel free to ping the programmers.');
                                    });
                                }).catch(() => {
                                  m.edit('Failed to create a new Entity. If your Entity exists already, please set your Entity ID with `++eid` instead.');
                                });
                            }).catch(oc => {
                                m.reactions.removeAll();
                                m.edit('An officer did not approve the requested action within one day.');
                            });
                        } else { // no
                            m.reactions.removeAll();
                            m.edit('Canceled. No Entities have been created and your Entity ID was not set.');
                        }
                    }).catch(c => {
                        m.reactions.removeAll();
                        m.edit('Please react within one minute with either Y or N.');
                    });
                });
            } else if (args.eid.match(/^s[0-9]{8}$/i)) { // leading S, probably bad
                msg.reply('If your Entity ID is your student ID, don\'t include a leading S.');
            } else { // unrecognized
                msg.reply('Your Entity ID does not appear to be a CdS student ID. Please contact an officer if this is incorrect.');
            }
        } else {
            msg.reply('Use the following syntax: `++setup 20400999 "John Doe"` (with your own ID and name)');
        }
    }
}

module.exports = CommandEid;