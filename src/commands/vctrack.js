const { Command } = require('discord-akairo');
const vc = require('../vc');

class CommandPing extends Command {
    constructor() {
        super('vctrack', {
            aliases: ['vctrack']
        })
    }

    exec(msg) {
        vc.setGuild(msg.guild);
        vc.setChannel(msg.channel);
        vc.startInterval();
        msg.reply('Tracking ' + vc.getCount() + ' voice channel(s).');
    }
}

module.exports = CommandPing;
