var fs = require('fs').promises;

async function get(discordid) {
  return fs.readFile('.in-n-out/discord/id/' + discordid).then(data => data.toString());
}

async function set(discordid, eid) {
  return fs.writeFile('.in-n-out/discord/id/' + discordid, eid);
}

module.exports = {
  get: get,
  set: set
};