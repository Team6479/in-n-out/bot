const fs = require('fs').promises;

var nextIsCustom = false;
var statuses = [];
var i = 0;

async function getStatuses() {
  return fs.readFile('.in-n-out/discord/statuses').then(data => data.toString().split('\n').map(ln => ln.split('|').map(s => s.trim())));
}

async function getNewStatus() {
  if (i === 0) {
    statuses = (await getStatuses()).sort(() => Math.random());
  }
  
  nextIsCustom = !nextIsCustom;
  i = (i + 1) % statuses.length;
  return nextIsCustom ? statuses[i] : ['PLAYING', 'help command coming soon'];
  
}

async function setNewStatus(client) {
  getNewStatus().then(s => client.user.setActivity(s[1], {type: s[0]}));
}

module.exports = {
  setNewStatus: setNewStatus
}