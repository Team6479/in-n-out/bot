const api = require('./api');
const eidb = require('./eidb');

var guild, channel, interval, tracking = {}, queue = {}, channelNames = {}, users = {}, tickCount = 0;

function setGuild(g) {
  guild = g;
}

function setChannel(c) {
  channel = c;
}

function getCount() {
  return guild.channels.cache.filter(c => c.type === 'voice').size;
}

function startInterval() {
  interval = setInterval(tick, 1000); // 1s resolution should be sufficient
}

function cancelInterval() {
  clearInterval(interval);
}

function get2Digit(n) {
  return Math.floor(n / 10).toString() + (n % 10).toString();
}

function getReadableDuration(ms) {
  let sec = Math.round(ms / 1000);
  const hr = Math.floor(sec / 3600);
  const min = Math.floor(sec / 60);
  sec %= 60;
  return hr + ':' + get2Digit(min) + ':' + get2Digit(sec);
}

function getReadableTime(ts) {
  return (new Date(ts)).toLocaleTimeString('en-GB');
}

function createMessage(cq, ts, end) {
  let msg = 'Automatically tracking meeting in voice channel: ' + cq.name + '\n';
  for (u in cq.sessions) {
    let total = 0;
    for (start in cq.sessions[u]) {
      total += (cq.sessions[u][start] || ts) - start;
    }
    msg += '- ' + users[u] + ': ' + getReadableDuration(total) + '\n';
  }
  // TODO: UTC to local
  // msg += 'The meeting started at ' + getReadableTime(cq.start) + '.';
  msg += end ? '\nAn officer will approve this meeting shortly.' : '';
  return msg;
}

async function complete_sessions(sessions, cancel, approver) {
  for (u in sessions) {
    await eidb.get(u).then(async eid => {
      for (start in sessions[u]) {
        if (cancel) {
          
          await api.cancel(eid, Math.floor(sessions[u][start] / 1000));
        } else {
          await api.signout(eid, Math.floor(sessions[u][start] / 1000), approver);
        }
      }
    }).catch(() => {}); // message already sent on join w/o eid, no need to repeat
  }
}

// FIXME: issues with starting a meeting when a previous meeting in the same channel has not been approved/canceled 

async function tick() {
  var ts = Date.now();
  guild.channels.cache.filter(c => c.type === 'voice').forEach(c => {
      channelNames[c.id] = c.name;
      tracking[c.id] = tracking[c.id] || {};
      c.members.forEach(u => {
          users[u.id] = u.toString();
          if (!tracking[c.id][u.id]) {
              tracking[c.id][u.id] = [ts, true]; // [timestamp, still here]
              if (queue[c.id] && !queue[c.id].done) {
                // users are only signed in here when the meeting is in progress
                // otherwise users who join early get extra time
                eidb.get(u.id).then(eid => {
                  api.signin(eid, Math.floor(ts / 1000), 'discord');
                }).catch(() => {
                  channel.send(u.toString() + ' You have not set your Entity ID. Please do so with `++eid`, or contact a programmer for help.');
                });
              }
          } else {
              tracking[c.id][u.id][1] = true; // still here
          }
      });
  });
  for (const c in tracking) {
    if (Object.keys(tracking[c]).length >= 2) {
      if (!queue[c]) {
        queue[c] = {
          name: channelNames[c],
          msg: await channel.send('Automatically tracking meeting in voice channel: ' + channelNames[c]),
          start: ts,
          sessions: {},
          done: false
        };
        for (u in tracking[c]) {
          // Revert all the start times of all tracked users' sessions
          // to the current timestamp once a meeting begins (user quorum met)
          // This prevents users who join early from gaining extra time
          tracking[c][u][0] = ts;

          // Users who join early are signed in once the meeting begins
          eidb.get(u).then(eid => {
            api.signin(eid, Math.floor(ts / 1000), 'discord');
          }).catch(() => {
            channel.send(users[u].toString() + ' You have not set your Entity ID. Please do so with `++eid`, or contact a programmer for help.');
          });
        }
      }
      for (const u in tracking[c]) {
          queue[c].sessions[u] = queue[c].sessions[u] || [];
          if (tracking[c][u][1]) {
              tracking[c][u][1] = false; // reset still here
              queue[c].sessions[u][tracking[c][u][0]] = ts;
          } else { // no longer in server
              eidb.get(u).then(eid => {
                queue[c].sessions[u][tracking[c][u][0]] = ts;
                delete tracking[c][u];
              }).catch(() => delete tracking[c][u]); // message already sent on join w/o eid, no need to repeat
          }
      }
      if (tickCount % 5 == 0) { // only edit every few ticks
        // higher frequency (lower mod) means more responsive but also more jittery
        await queue[c].msg.edit(createMessage(queue[c], ts, false));
      }
    } else {
      if (queue[c] && !queue[c].done) {
        queue[c].done = true;
        const officer_filter = (reaction, user) => ['✅', '❌'].includes(reaction.emoji.name) && guild.member(user).roles.cache.some(r => r.name === 'Officer');
        await queue[c].msg.edit(createMessage(queue[c], ts, true)).then(() => {
          queue[c].msg.react('✅').then(() => queue[c].msg.react('❌'));
          queue[c].msg.awaitReactions(officer_filter, {max: 1, time: 24 * 3600 * 1000, errors: ['time']}).then(rl => {
            const r = rl.first();
            let approver = null;
            r.users.cache.map(u => {
              if (guild.member(u).roles.cache.some(r => r.name === 'Officer')) {
                approver = u;
              }
            });
            queue[c].msg.reactions.removeAll();
            if (r.emoji.name === '✅') { // approve
              queue[c].msg.edit('Meeting approved. Submitting to In-N-Out...').then(() => {
                eidb.get(approver.id).then(eid => {
                  complete_sessions(queue[c].sessions, false, eid).then(() => {
                    queue[c].msg.edit('A automatically tracked metting in ' + queue[c].name
                        // + ' starting at ' + getReadableTime(queue[c].start)
                        + ' with ' + Object.keys(queue[c].sessions).length
                        + ' members was submitted to In-N-Out.\n'
                        + 'It was approved by ' + approver.toString())
                        .then(() => delete queue[c]);
                  });
                }).catch(() => {
                  complete_sessions(queue[c].sessions, false, null).then(() => {
                    queue[c].msg.edit('A automatically tracked metting in ' + queue[c].name
                        // + ' starting at ' + getReadableTime(queue[c].start)
                        + ' with ' + Object.keys(queue[c].sessions).length
                        + ' members was submitted to In-N-Out.\n'
                        + approver.toString() + ' has not set their EID.')
                        .then(() => delete queue[c]);
                  });
                })
              });
            } else { // do not approve
              queue[c].msg.edit('Meeting disapproved. Canceling...').then(() => {
                complete_sessions(queue[c].sessions, true, null).then(() => {
                  queue[c].msg.edit('A automatically tracked metting in ' + queue[c].name
                      // + ' starting at ' + getReadableTime(queue[c].start)
                      + ' with ' + Object.keys(queue[c].sessions).length
                      + ' members was manually canceled by '
                      + approver.toString() + '.')
                      .then(() => delete queue[c]);
                });
              });
            }
          }).catch(() => {
            queue[c].msg.reactions.removeAll();
            queue[c].msg.edit('Approval timed out. Canceling...').then(() => {
              complete_sessions(queue[c].sessions, true, null).then(() => {
                queue[c].msg.edit('A automatically tracked metting in ' + queue[c].name
                    // + ' starting at ' + getReadableTime(queue[c].start)
                    + ' with ' + Object.keys(queue[c].sessions).length
                    + ' members was canceled.\n'
                    + 'An officer must approve this meeting within 24hrs of the meeting ending.')
                    .then(() => delete queue[c]);
              });
            });
          });
        });
      }
    }
  }
  tickCount++;
}

module.exports = {
  setGuild: setGuild,
  setChannel: setChannel,
  getCount: getCount,
  startInterval: startInterval,
  cancelInterval: cancelInterval
};